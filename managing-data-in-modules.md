## Managing Data in Modules  (https://www.youtube.com/watch?v=E8-e-3fRHBw)
#### Speaker : Randy Shoup

If we think of each module as a microservice, then any other module should only be able to touch the state in this module's persistence through the module's published interface (API, events or ETL published). 

While this sounds good in theory, it may be difficult to do this in real-world applications. Below are some pointers on how we can achieve this to a reasonably large extent. But before we do that, let's define the principles that we must follow for being able to do this effectively.

### Principles
1. Every interesting piece of data should be owned by a single service. That service is the canonical system of record for that data. If the data is cached by multiple services, each such copy is a read-only non-authoritative cache.


#### Shared Data
**Problem**: In a monolithic application, it is very easy to leverage shared data. Where does shared data go in a microservices world?

**Approach 1** : Synchronous lookup
    - Any service that needs some piece of information can simply look it up from the data's system of record (governed by Principle 1).

**Approach 2** : Async event + local cache
    - Service 1 owns data A
    - Service 1 sends A-updates event
    - Service 2 consumes event, updates local cache for A 

**Approach 3** : Shared metadata library 
    - For shared metadata, which are not changed too often (e.g. loan credit rating bands, country codes etc)

#### Joins
**Problem**: In a monolithic application, it is very easy to join data across tables. Splitting data across services makes joins very hard. But we still want to be able to do joins.

**Approach 1** : Join in Client Application
    - Get left data from service A
    - Get right data from service B matching all rows in left data
    - Ideal for 1:N joins

**Approach 2** : Materialized View
    - Listen to events that talk about the data your service wants to join on
    - Maintain a denormalized join in local storage
    - Ideal for M:N joins


#### Transactions
**Problem** : In a monolithic application, running a transaction spanning multiple entities is very easy. Splitting a database across services makes transactions very hard.
> Two-phase commits are really difficult to do in a distributed setting such as microservices. It kills performance and the scalability of your application.
> Instead of thinking of it as an atomic transaction, think of it as a saga (a workflow of individual atomic transactions and maintain a kind of state machine as to what the current state is). Rollback is by applying compensating actions that undo the transaction in the reverse direction of the workflow. More info on what is a saga at Katie McCaffrey's talks on the topic.
> Systems where this happens: payments processing, expense approval, almost any kind of workflow etc.


### What about events coming out of order, multiple times?
> Systems can be designed to have _at-most-once_ delivery or _at-least-once_ delivery. Design systems to be at-most-once delivery if you don't really care about that thing. e.g. logging solutions: it shouldn't matter much if you lose some logs since you don't care much for them!
> In most cases, for business systems, we would need to use at-least-once delivery. This leads to two problems: I will get the same event twice and I might get them out of order. 
    > Multiple duplicate events can be handled if you can make your consumers idempotent. 
    > For out of order events, first consider if you care about the ordering of events in your particular case. If out of order is okay, remaining stateless should be good enough. If you do care about the order, then the server/consumer would have to keep some sort of state (CRDTs)