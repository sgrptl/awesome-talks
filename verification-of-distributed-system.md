## Verification of a Distributed System (https://www.youtube.com/watch?v=kDh5BrqiGhI)
#### Speaker : Caitie McCaffrey

### Formal verification
TLA+ and Coq
They test the specification, not the implementation

### Testing in the wild
Unit tests : for the modules in your implementation.

Integration tests : for testing the integrated modules to verify combined functionality.

"Simple testing Can prevent most critical failures" - Paper from 2014
    - 98% of all failures can be reproduced on three or less nodes
    - testing error handling code could have prevented 58% of catastrophic failures (code coverage tool)
    - 35% of catastrophic failures consist of:
        - error handling code is simply empty or contains a log statement
        - error handler aborts cluster on an overly general exception ()
        - error handler contains comments like FIXME or TODO

Property based testing : 

Fault injection : 
    - Without explicity forcing a system to fail, it is unreasonable to have any confidence it will operate correctly in failure modes 
    - Netflix Simian Army, Jepsen



### Research